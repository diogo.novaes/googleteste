
package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions
        (
                features = "src/feature",
                tags = "@LG",
                glue = "src/test/steps/Steps"
                )
public class Runner {

}