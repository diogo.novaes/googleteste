package steps;

import Unit.Utilit;
import io.cucumber.java.es.Dado;
import io.cucumber.java.it.Quando;
import io.cucumber.java.pt.Entao;

public class Steps {
    Utilit utilit = new Utilit();


    @Dado("que o usuario logue no {string}")
    public void que_o_usuario_logue_no(String urls) {
        utilit.onAir(urls);


    }

    @Quando("quando o usuario realizar o Login passando o {string} e a {string}")
    public void quando_o_usuario_realizar_o_Login_passando_o_e_a(String usuario, String senha) {
        utilit.RealizarLogin(usuario,senha);
    }

    @Entao("o cenario sera validado")
    public void o_cenario_sera_validado() {

    }

}
