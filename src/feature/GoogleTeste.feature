#language: pt

Funcionalidade: :Login google

  @LG
  Esquema do Cenario:Valindo o Login do Goole
    Dado que o usuario logue no "<urls>"
    Quando quando o usuario realizar o Login passando o "<usuario>" e a "<senha>"
    Entao o cenario sera validado


    Exemplos:
      | urls                  | usuario                    | senha      |
      | htpps://google.com.br | diogoramosnovaes@gmail.com | hnon030508 |